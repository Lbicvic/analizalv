﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Kalkulator : Form
    {
        public Kalkulator()
        {
            InitializeComponent();
        }
        double op1,op2, num;
        int operators;
        public void calculate()
        {
           
                switch (operators)
                {
                    case 1:
                        op2 = double.Parse(textBox1.Text);
                        num = op1 + op2;
                        textBox1.Text = num.ToString();
                        break;
                    case 2:
                        op2 = double.Parse(textBox1.Text);
                        num = op1 - op2;
                        textBox1.Text = num.ToString();
                        break;
                    case 3:
                        op2 = double.Parse(textBox1.Text);
                        num = op1 * op2;
                        textBox1.Text = num.ToString();
                        break;
                    case 4:
                        op2 = double.Parse(textBox1.Text);
                        num = op1 / op2;
                        textBox1.Text = num.ToString();
                        break;
                    default:
                        break;
                }

         

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnJednako_Click(object sender, EventArgs e)
        {
            calculate();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            textBox1.Text += 0;
           
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            textBox1.Text += 1;
            
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            textBox1.Text += 2;
            
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            textBox1.Text += 3;
          
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            textBox1.Text += 4;
           
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            textBox1.Text += 5;
           
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            textBox1.Text += 6;
           
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            textBox1.Text += 7;
          
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            textBox1.Text += 8;
           
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            textBox1.Text += 9;
            
        }

        private void btnTocka_Click(object sender, EventArgs e)
        {
            textBox1.Text += '.';
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            op1 = double.Parse(textBox1.Text);
            operators = 1;
            textBox1.Clear();
        }

        private void btn_odz_Click(object sender, EventArgs e)
        {
            op1 = double.Parse(textBox1.Text);
            operators = 2;
            textBox1.Clear();
        }

        private void btn_put_Click(object sender, EventArgs e)
        {
            op1 = double.Parse(textBox1.Text);
            operators = 3;
            textBox1.Clear();
        }

        private void btn_djelj_Click(object sender, EventArgs e)
        {
            op1 = double.Parse(textBox1.Text);
            operators = 4;
            textBox1.Clear();
        }

        private void btn_sin_Click(object sender, EventArgs e)
        {
            double sinus = double.Parse(textBox1.Text);
            textBox1.Text = Math.Sin(sinus).ToString();
        }

        private void btn_cos_Click(object sender, EventArgs e)
        {
            double cosinus = double.Parse(textBox1.Text);
            textBox1.Text = Math.Cos(cosinus).ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            double tangens = double.Parse(textBox1.Text);
            textBox1.Text = Math.Tan(tangens).ToString();
        }

        private void btn_sqrt_Click(object sender, EventArgs e)
        {
            double korijen = double.Parse(textBox1.Text);
            textBox1.Text = Math.Sqrt(korijen).ToString();
        }

        private void btn_log_Click(object sender, EventArgs e)
        {
            double logaritam = double.Parse(textBox1.Text);
            textBox1.Text = Math.Log10(logaritam).ToString();
        }

       







    }
}
