﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp10
{
    public partial class Form1 : Form
    {
        Random rand = new Random();
        List<string> list = new List<string>();
        int broj_pokusaja;
        string rijeclista, rijeclabel;
        string path = "vjesalo.txt";

        public void Reset()
        {
            rijeclista = list[rand.Next(0, list.Count - 1)];
            rijeclabel = new string('*', rijeclista.Length);
            label1.Text = rijeclabel;
            broj_pokusaja = 5;
            label2.Text = broj_pokusaja.ToString();
        }
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string line;
            using (System.IO.StreamReader reader = new System.IO.StreamReader
           (@path))
            {
                while ((line = reader.ReadLine()) != null)
                {
                    list.Add(line);
                }
                Reset();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length == 1)
            {
                if (rijeclista.Contains(textBox1.Text))
                {
                    string temp_rijecx = rijeclista;
                    while (temp_rijecx.Contains(textBox1.Text))
                    {
                        int index = temp_rijecx.IndexOf(textBox1.Text);
                        StringBuilder builder = new StringBuilder(temp_rijecx);
                        builder[index] = '*';
                        temp_rijecx = builder.ToString();

                        StringBuilder builder2 = new StringBuilder(rijeclabel);
                        builder2[index] = Convert.ToChar(textBox1.Text);
                        rijeclabel = builder2.ToString();
                    }
                    label1.Text = rijeclabel;
                    if (rijeclabel == rijeclista)
                    {
                        MessageBox.Show("Pogodili ste trazenu rijec ! Pobijeda !!!!");
                        Reset();
                    }
                }
                else
                {
                    broj_pokusaja--;
                    label2.Text = broj_pokusaja.ToString();
                    if (broj_pokusaja <= 0)
                    {
                        MessageBox.Show("Nazalost ste izgubili :(\nRijec koja je bila trazena : " + rijeclista);
                        Reset();
                    }
                }
            }
            else if (textBox1.Text.Length > 1)
            {
                if (rijeclista == textBox1.Text)
                {
                    MessageBox.Show("Pogodili ste trazenu rijec ! Pobijeda !");
                    Reset();
                }
                else
                {
                    broj_pokusaja--;
                    label2.Text = broj_pokusaja.ToString();
                    if (broj_pokusaja <= 0)
                    {
                        MessageBox.Show("Nazalost ste izgubili :(\nRijec koja je bila trazena : " + rijeclista);
                        Reset();
                    }
                }
            } 
        }
        
    }
}
